//
//  Endpoint.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import Foundation
import Alamofire

enum Endpoint {
    static let baseUrlString = "https://green-thumb-64168.uc.r.appspot.com/"
    
    enum Login {
        case postLogin(String, String)
        
        func build() -> URLRequestConvertible {
            switch self {
            case .postLogin(let username, let password):
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)login", method: .post, parameter: ["username" : username, "password" : password])
            }
        }
    }
    
    enum Register {
        case postRegister(String, String)
        
        func build() -> URLRequestConvertible {
            switch self {
            case .postRegister(let username, let password):
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)register", method: .post, parameter: ["username" : username, "password" : password])
            }
        }
    }
    
    enum Home {
        case getContact
        case getBalance
        case getTransactionList
        
        func build() -> URLRequestConvertible {
            switch self {
            case .getContact:
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)payees", method: .get, parameter: nil)
            case .getBalance:
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)balance", method: .get, parameter: nil)
            case .getTransactionList:
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)transactions", method: .get, parameter: nil)
            }
        }
    }
    
    enum Transfer {
        case postTransfer(String, Int, String)
        
        func build() -> URLRequestConvertible {
            switch self {
            case.postTransfer(let accountNo, let amount, let description):
                let parameters: [String : Any] = [
                    "receipientAccountNo" : accountNo,
                    "amount" : amount,
                    "description" : description
                ]
                return Network.shared().createUrlRequest(url: "\(Endpoint.baseUrlString)transfer", method: .post, parameter: parameters)
            }
        }
    }
}
