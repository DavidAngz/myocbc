//
//  Network.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import Foundation
import Alamofire
import SwiftyJSON

class Network {
    
    let manager = Alamofire.Session.default
    
    private static var network = Network()
    
    private init() { }
    
    class func shared() -> Network {
        return network
    }
    
    func request(endpoint: URLRequestConvertible, completion : @escaping (_ success : JSON?, _ failure : Error?) -> Void) {
        AF.request(endpoint)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let success = self.onSuccess(value: value)
                completion(success.0, success.1)
                break
            case .failure(let error):
                if let data = response.data {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                        let error = self.onError(value: json, error: error)
                        completion(error.0, error.1)
                    }
                    return
                }
                let error = self.onError(value: nil, error: error)
                completion(nil, error.1)
                
                break
            }
        }
    }
    
    func upload(url : String, imageParam: String, imageData: Data?, parameters: [String : Any], onProgress: (@escaping (UploadRequest) -> Void), onComplete: @escaping (JSON?) -> Void, onError: @escaping ((Error?) -> Void)) {
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: imageParam, fileName: "file.jpg", mimeType: "image/jpg")
            }
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: [
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Authorization" : "Bearer " + (Keychain.token.load() ?? "")
        ]).responseJSON { response in
            switch response.result {
            case .success(let value) :
                onComplete(JSON(value))
                break
            case .failure(let error) :
                onError(error)
                break
            }
        }
    }
    
    func onSuccess(value : Any) -> (JSON?, Error?) {
        let json = JSON(value)
        return (json, nil)
    }
    
    func onError(value : Any?, error : Error) -> (JSON?, Error?) {
        if value != nil {
            let json = JSON(value!)
            return (json, error)
        } else {
            return (nil, error)
        }
    }
    
    func createUrlRequest(url: String, method: HTTPMethod, parameter : Parameters?) -> URLRequest {
        let requestUrl = URLRequest(url: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!)
        let jsonEncoding = URLEncoding.httpBody
        var encoding = try! jsonEncoding.encode(requestUrl, with: parameter)
//        encoding.parameter
        encoding.httpMethod = method.rawValue
        encoding.allHTTPHeaderFields = headers()
        encoding.timeoutInterval = 60
        return encoding
    }
    
    private func headers() -> [String : String]{
        return [
            "Content-Type" : "application/x-www-form-urlencoded",
            "Accept" : "application/json",
            "Authorization" : Keychain.token.load() ?? ""
        ]
    }
    
//    private func multipartHeaders() -> HTTPHeaders {
//        return [
//            "Content-Type": "multipart/form-data",
//            "Accept" : "application/json",
//            "Authorization": "Bearer " + Account.standard.getStrValue(Account.Value.access_token)
//        ]
//    }
}
