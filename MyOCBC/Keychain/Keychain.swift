//
//  Keychain.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import Foundation

enum Keychain : NSString, CaseIterable {
    
    case token = "token"
    case name = "name"
    case cardNumber = "cardNumber"
    case balance = "balance"
    
    func save(data: String?) {
        KeychainManager.save(self.rawValue, data: data! as NSString)
    }
    
    func load() -> String? {
        return KeychainManager.load(self.rawValue) as? String
    }
    
    func delete() {
        KeychainManager.delete(self.rawValue)
    }
    
}
