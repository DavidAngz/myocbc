//
//  KeychainManager.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import Foundation
import Security
import UIKit
import CoreLocation

let kSecClassValue = kSecClass as NSString
let kSecValueDataValue = kSecValueData as NSString
let kSecMatchLimitValue = kSecMatchLimit as NSString
let kSecReturnDataValue = kSecReturnData as NSString
let kSecAttrAccountValue = kSecAttrAccount as NSString
let kSecAttrServiceValue = kSecAttrService as NSString
let kSecMatchLimitOneValue = kSecMatchLimitOne as NSString
let kSecClassGenericPasswordValue = kSecClassGenericPassword as NSString

class KeychainManager: NSObject
{
    class func save (_ saveData: NSString, data: NSString)
    {
        let dataFromString: Data = data.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!
        
        let keychainQuery = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, saveData, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecValueDataValue])
        
        if SecItemCopyMatching(keychainQuery as CFDictionary, nil) == noErr
        {
            SecItemDelete(keychainQuery as CFDictionary)
        }
        else
        {
            let status: OSStatus = SecItemAdd(keychainQuery as CFDictionary, nil)
            print("Saving status code is: \(status)")
        }
    }
    
    class func delete (_ key: NSString)
    {
        let keychainQuery = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, key], forKeys: [kSecClassValue, kSecAttrServiceValue])
        let status : OSStatus = SecItemDelete(keychainQuery as CFDictionary)
        print(status)
    }
    
    class func load (_ service: NSString) -> AnyObject?
    {
        var result : String!
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecReturnDataValue, kSecMatchLimitValue])
        
        var dataTypeRef :AnyObject?
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        
        if status == errSecSuccess
        {
            let retrievedData : Data? = dataTypeRef as? Data
            result = String (data: retrievedData!, encoding: String.Encoding.utf8)
            return result as AnyObject?
        }
        else
        {
            return nil
        }
    }
}
