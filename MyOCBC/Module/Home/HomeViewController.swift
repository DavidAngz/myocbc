//
//  HomeViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SkeletonView

class HomeViewController: UIViewController {
    let root = HomeView()
    lazy var presenter: HomePresenterDelegate = HomePresenter(view: self)
    var contactList: [ContactListModel] = []
    var transactionList: [TransactionModel] = []
    
    override func viewDidLoad() {
        super .viewDidLoad()
        view = root
        
        setCollectionDelegate()
        root.showSkeleton()
        root.helloLbl.text = "Hello \(Keychain.name.load() ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        presenter.getContactList()
        presenter.getBalance()
        presenter.getTransactionList()
    }
    
    func setCollectionDelegate() {
        root.contactCV.delegate = self
        root.contactCV.dataSource = self
        
        root.transactionCV.delegate = self
        root.transactionCV.dataSource = self
    }
}

extension HomeViewController: SkeletonCollectionViewDelegate, SkeletonCollectionViewDataSource {
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "cell"
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.contactList.count
        } else {
            return self.transactionList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ContactCell
            cell.initialize(model: self.contactList[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TransactionCell
            cell.initialize(model: self.transactionList[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            let model = self.contactList[indexPath.row]
            self.presenter.pushToSendMoney(name: model.name, accountNo: model.accountNo)
        }
    }
}

extension HomeViewController: HomeViewDelegate {
    func getContactListOnSuccess(model: [ContactListModel]) {
        self.contactList = model
        root.contactCV.hideSkeleton(transition: .crossDissolve(1.5))
        root.contactCV.reloadData()
    }
    
    func getContactListOnFailure(message: String) {
        
    }
    
    func getBalanceOnSuccess(balance: Int) {
        root.balanceValueLbl.hideSkeleton(transition: .crossDissolve(1.5))
        root.balanceValueLbl.text = "SGD \(balance)"
        Keychain.balance.delete()
        Keychain.balance.save(data: "\(balance)")
    }
    
    func getBalanceOnFailure(message: String) {
        
    }
    
    func getTransactionListOnSuccess(data: [TransactionModel]) {
        root.transactionCV.hideSkeleton(transition: .crossDissolve(1.5))
        transactionList = data
        root.transactionCV.reloadData()
        root.transactionCV.updateHeight(numberOfItemsInRow: 1, totalData: data.count)
    }
    
    func getTransactionListOnFailure(message: String) {
        
    }
}
