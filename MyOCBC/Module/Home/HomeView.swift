//
//  HomeView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit
import SkeletonView

class HomeView: UIView {
    let scrollView = UIScrollView()
    let mainView = UIView()
    let helloLbl = UILabel()
    let welcomeLbl = UILabel()
    
    let currentBalanceView = UIView()
    let currentBalanceLbl = UILabel()
    let balanceValueLbl = UILabel()
    
    let quickSendLbl = UILabel()
    let contactCV = UICollectionView(frame: CGRect(), collectionViewLayout: UICollectionViewFlowLayout())
    
    let recentActivityLbl = UILabel()
    let transactionCV = UICollectionView(frame: CGRect(), collectionViewLayout: UICollectionViewFlowLayout())
    
    private func contactCVLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewFlowLayout()
        let cellHeightConstant: CGFloat = 50
        let cellWidthConstant: CGFloat = 145

        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 12
        layout.itemSize = CGSize(width: cellWidthConstant, height: cellHeightConstant)
        
        return layout
    }
    
    private func transactionCVLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewFlowLayout()
        let cellHeightConstant: CGFloat = 104
        let cellWidthConstant: CGFloat = mainScreen.width

        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 8
        layout.itemSize = CGSize(width: cellWidthConstant, height: cellHeightConstant)

        return layout
    }
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([scrollView])
        scrollView.addContentView(view: mainView)
        mainView.setupSubviews([helloLbl, welcomeLbl, currentBalanceView, quickSendLbl, contactCV, recentActivityLbl, transactionCV])
        currentBalanceView.setupSubviews([currentBalanceLbl, balanceValueLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showSkeleton() {
        contactCV.showAnimatedSkeleton()
        balanceValueLbl.showAnimatedSkeleton()
        transactionCV.showAnimatedSkeleton()
    }
    
    func setupView() {
        scrollView.showsVerticalScrollIndicator = false
        
        helloLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 16)
            $0.textColor = .OCBC.mainTextColor
            $0.text = "Hello David"
        }
        
        welcomeLbl.style {
            $0.font = UIFont.MontserratBold(ofSize: 20)
            $0.textColor = .OCBC.mainTextColor
            $0.text = "Welcome Back!"
        }
        
        currentBalanceView.style {
            $0.backgroundColor = .OCBC.pink
            $0.setLayer(cornerRadius: 8 , clipToBounds: true)
            $0.layer.shadowOffset = CGSize(width: 3,
                                              height: 3)
            $0.layer.shadowRadius = 3
            $0.layer.shadowOpacity = 0.2
            $0.layer.shadowColor = UIColor.OCBC.lightGray.cgColor
            $0.layer.masksToBounds = false
        }
        
        currentBalanceLbl.style {
            $0.text = "Current Balance"
            $0.textColor = .OCBC.mainRed
            $0.font = UIFont.MontserratSemiBold(ofSize: 16)
        }
        
        balanceValueLbl.style {
            $0.text = "$4,570.80"
            $0.textColor = .OCBC.secondaryRed
            $0.font = UIFont.MontserratBold(ofSize: 24)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        quickSendLbl.style {
            $0.text = "Quick send"
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratSemiBold(ofSize: 16)
        }
        
        contactCV.style {
            $0.tag = 0
            $0.collectionViewLayout = self.contactCVLayout()
            $0.showsHorizontalScrollIndicator = false
            $0.register(ContactCell.self, forCellWithReuseIdentifier: "cell")
            $0.isSkeletonable = true
        }
        
        recentActivityLbl.style {
            $0.text = "Recent activity"
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratSemiBold(ofSize: 16)
        }
        
        transactionCV.style {
            $0.tag = 1
            $0.collectionViewLayout = self.transactionCVLayout()
            $0.showsVerticalScrollIndicator = false
            $0.register(TransactionCell.self, forCellWithReuseIdentifier: "cell")
            $0.isScrollEnabled = false
            $0.isSkeletonable = true
        }
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalTo(self)
            make.bottom.equalTo(self).offset(-100)
        }
        
        helloLbl.snp.makeConstraints { make in
            make.top.equalTo(mainView).offset(48)
            make.leading.equalTo(mainView).offset(20)
            make.trailing.equalTo(mainView).offset(-20)
        }
        
        welcomeLbl.snp.makeConstraints { make in
            make.top.equalTo(helloLbl.snp.bottom).offset(8)
            make.leading.trailing.equalTo(helloLbl)
//            make.bottom.equalTo(mainView)
        }
        
        currentBalanceView.snp.makeConstraints { make in
            make.leading.equalTo(mainView).offset(20)
            make.trailing.equalTo(mainView).offset(-20)
            make.top.equalTo(welcomeLbl.snp.bottom).offset(40)
        }
        
        currentBalanceLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(currentBalanceView).offset(20)
        }
        
        balanceValueLbl.snp.makeConstraints { make in
            make.top.equalTo(currentBalanceLbl.snp.bottom).offset(8)
            make.leading.equalTo(currentBalanceLbl)
            make.trailing.equalTo(currentBalanceView).offset(-20)
            make.bottom.equalTo(currentBalanceView).offset(-24)
        }
        
        quickSendLbl.snp.makeConstraints { make in
            make.top.equalTo(currentBalanceView.snp.bottom).offset(32)
            make.leading.equalTo(mainView).offset(20)
        }
        
        contactCV.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.equalTo(quickSendLbl)
            make.top.equalTo(quickSendLbl.snp.bottom).offset(16)
            make.trailing.equalTo(mainView)
        }
        
        recentActivityLbl.snp.makeConstraints { make in
            make.leading.equalTo(quickSendLbl)
            make.top.equalTo(contactCV.snp.bottom).offset(32)
        }
        
        transactionCV.snp.makeConstraints { make in
            make.height.equalTo(500)
            make.top.equalTo(recentActivityLbl.snp.bottom).offset(16)
            make.leading.trailing.equalTo(mainView)
            make.bottom.equalTo(mainView)
        }
    }
}
