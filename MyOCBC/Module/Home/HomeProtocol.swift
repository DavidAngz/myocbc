//
//  HomeProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation

protocol HomeViewDelegate: AnyObject {
    func getContactListOnSuccess(model: [ContactListModel])
    func getContactListOnFailure(message: String)
    
    func getBalanceOnSuccess(balance: Int)
    func getBalanceOnFailure(message: String)
    
    func getTransactionListOnSuccess(data: [TransactionModel])
    func getTransactionListOnFailure(message: String)
}

protocol HomePresenterDelegate: AnyObject {
    func getContactList()
    func getContactListOnSuccess(model: [ContactListModel])
    func getContactListOnFailure(message: String)
    
    func getBalance()
    func getBalanceOnSuccess(balance: Int)
    func getBalanceOnFailure(message: String)
    
    func getTransactionList()
    func getTransactionListOnSuccess(data: [TransactionModel])
    func getTransactionListOnFailure(message: String)
    
    func pushToSendMoney(name: String, accountNo: String)
}

protocol HomeInteractorDelegate: AnyObject {
    func getContactList()
    func getBalance()
    func getTransactionList()
}

protocol HomeRouterDelegate: AnyObject {
    func pushToSendMoney(name: String, accountNo: String)
}
