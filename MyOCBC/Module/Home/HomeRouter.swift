//
//  HomeRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

final class HomeRouter: HomeRouterDelegate {

    weak var source: UIViewController?
    
    init(view: HomeViewDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToSendMoney(name: String, accountNo: String) {
        let vc = TransferViewController()
        vc.root.recipientTF.textfield.text = name
        vc.selectedAccountNo = accountNo
        source?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
