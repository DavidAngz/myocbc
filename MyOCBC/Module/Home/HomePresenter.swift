//
//  HomePresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit

final class HomePresenter: HomePresenterDelegate {
 
    weak var view: HomeViewDelegate?
    lazy var interactor: HomeInteractorDelegate = HomeInteractor(presenter: self)
    lazy var router: HomeRouterDelegate? = HomeRouter(view: view)
    
    init(view: HomeViewDelegate?) {
        self.view = view
    }
    
    func getContactList() {
        interactor.getContactList()
    }
    
    func getContactListOnSuccess(model: [ContactListModel]) {
        view?.getContactListOnSuccess(model: model)
    }
    
    func getContactListOnFailure(message: String) {
        view?.getContactListOnFailure(message: message)
    }
    
    func getBalance() {
        interactor.getBalance()
    }
    
    func getBalanceOnSuccess(balance: Int) {
        view?.getBalanceOnSuccess(balance: balance)
    }
    
    func getBalanceOnFailure(message: String) {
        view?.getBalanceOnFailure(message: message)
    }
    
    func pushToSendMoney(name: String, accountNo: String) {
        router?.pushToSendMoney(name: name, accountNo: accountNo)
    }
    
    func getTransactionList() {
        interactor.getTransactionList()
    }
    
    func getTransactionListOnSuccess(data: [TransactionModel]) {
        view?.getTransactionListOnSuccess(data: data)
    }
    
    func getTransactionListOnFailure(message: String) {
        view?.getTransactionListOnFailure(message: message)
    }
    
}
