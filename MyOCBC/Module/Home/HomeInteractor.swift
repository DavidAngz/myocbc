//
//  HomeInteractor.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit
import SwiftyJSON
import Alamofire

final class HomeInteractor: HomeInteractorDelegate {

    weak var presenter: HomePresenterDelegate?
    
    init(presenter : HomePresenterDelegate?) {
        self.presenter = presenter
    }
    
    func getContactList() {
        let endpoint = Endpoint.Home.getContact.build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.getContactListResponse(response: success, error: failure)
        }
    }
    
    func getContactListResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = BaseModel<ContactListModel>(response)
            if error == nil {
                if model.status {
                    self.presenter?.getContactListOnSuccess(model: model.dataArray)
                } else {
                    self.presenter?.getContactListOnFailure(message: model.error)
                }
            } else {
                self.presenter?.getContactListOnFailure(message: model.error)
            }
        } else {
            self.presenter?.getContactListOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
    func getBalance() {
        let endpoint = Endpoint.Home.getBalance.build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.getBalanceResponse(response: success, error: failure)
        }
    }
    
    func getBalanceResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = BalanceModel(response)
            if error == nil {
                if model.status {
                    self.presenter?.getBalanceOnSuccess(balance: model.balance)
                } else {
                    self.presenter?.getBalanceOnFailure(message: model.error)
                }
            } else {
                self.presenter?.getBalanceOnFailure(message: model.error)
            }
        } else {
            self.presenter?.getBalanceOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
    func getTransactionList() {
        let endpoint = Endpoint.Home.getTransactionList.build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.getTransactionListResponse(response: success, error: failure)
        }
    }
    
    func getTransactionListResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = BaseModel<TransactionModel>(response)
            if error == nil {
                if model.status {
                    self.presenter?.getTransactionListOnSuccess(data: model.dataArray)
                } else {
                    self.presenter?.getTransactionListOnFailure(message: model.error)
                }
            } else {
                self.presenter?.getTransactionListOnFailure(message: model.error)
            }
        } else {
            self.presenter?.getTransactionListOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
}
