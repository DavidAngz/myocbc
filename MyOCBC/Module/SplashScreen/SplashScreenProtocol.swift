//
//  SplashScreenProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 15/07/22.
//

import Foundation

protocol SplashScreenViewDelegate: AnyObject {
    
}

protocol SplashScreenPresenterDelegate: AnyObject {
    func pushToLoginView()
    func pushToHomeView()
}

protocol SplashScreenRouterDelegate: AnyObject {
    func pushToLoginView()
    func pushToHomeView()
}
