//
//  SplashScreenPresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 15/07/22.
//

import UIKit

final class SplashScreenPresenter: SplashScreenPresenterDelegate {
    
    weak var view: SplashScreenViewDelegate?
    lazy var router: SplashScreenRouterDelegate? = SplashScreenRouter(view: view)
    
    init(view: SplashScreenViewDelegate?) {
        self.view = view
    }
    
    func pushToLoginView() {
        router?.pushToLoginView()
    }
    
    func pushToHomeView() {
        router?.pushToHomeView()
    }
}
