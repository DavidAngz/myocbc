//
//  SplashScreenViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 15/07/22.
//

import UIKit

class SplashScreenViewController: UIViewController, SplashScreenViewDelegate {
    let root = SplashScreenView()
    lazy var presenter: SplashScreenPresenterDelegate = SplashScreenPresenter(view: self)
    
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super .viewDidLoad()
        view = root
        
        animateLogo()
//        setTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        root.ocbcLogoImgView.alpha = 1
        setTimer()
    }
    
    func animateLogo() {
        let flash = CASpringAnimation(keyPath: "opacity")
        flash.duration = 0.6
        flash.fromValue = 0.75
        flash.toValue = 1.0
        flash.autoreverses = true
        flash.repeatCount = 1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.6
        pulse.fromValue = 0.99
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        root.ocbcLogoImgView.layer.add(flash, forKey: nil)
        root.ocbcLogoImgView.layer.add(pulse, forKey: nil)
    }
    
    @objc func setPush() {
        UIView.animate(withDuration: 0.7, animations: {
            self.root.ocbcLogoImgView.alpha = 0.0
        }) { (_) in
            if self.userDefaults.bool(forKey: "app_hasRunBefore") == true {
                if Keychain.token.load() != nil {
                    self.presenter.pushToHomeView()
                } else {
                    self.presenter.pushToLoginView()
                }
            } else {
                Keychain.token.delete()
                self.userDefaults.set(true, forKey: "app_hasRunBefore")
                self.presenter.pushToLoginView()
            }
        }
    }
    
    func setTimer() {
        Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(self.setPush), userInfo: nil, repeats: false)
    }
}
