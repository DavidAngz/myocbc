//
//  SplashScreenRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 15/07/22.
//

import UIKit

final class SplashScreenRouter: SplashScreenRouterDelegate {

    weak var source: UIViewController?
    
    init(view: SplashScreenViewDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToLoginView() {
        let loginView = LoginViewController()
        source?.navigationController?.pushViewController(loginView, animated: true)
    }
    
    func pushToHomeView() {
        let mainMenu = NavigationMenuBaseController()
        source?.navigationController?.pushViewController(mainMenu, animated: true)
    }
}
