//
//  SplashScreenView.swift
//  MyOCBC
//
//  Created by David Angkasa on 15/07/22.
//

import UIKit
import SnapKit

class SplashScreenView: UIView {
    let ocbcLogoImgView = UIImageView()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .OCBC.secondaryRed
        
        setupSubviews([ocbcLogoImgView])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        ocbcLogoImgView.image = UIImage(named: "ic_ocbc_logo")
    }
    
    func setupConstraints() {
        ocbcLogoImgView.snp.makeConstraints { make in
            make.width.equalTo(240)
            make.height.equalTo(162)
            make.center.equalTo(self)
        }
    }
}
