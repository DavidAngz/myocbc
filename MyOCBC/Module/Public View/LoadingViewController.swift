//
//  LoadingViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit

class LoadingViewController : UIViewController {
    let root = LoadingView()
    
    override func viewDidLoad() {
        super .viewDidLoad()
        view = root
        root.animationView.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        root.animationView.stop()
    }
}
