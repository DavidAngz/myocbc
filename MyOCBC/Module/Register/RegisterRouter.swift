//
//  RegisterRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit

final class RegisterRouter: RegisterRouterDelegate {

    weak var source: UIViewController?
    
    init(view: RegisterViewDelegate?) {
        source = view as? UIViewController
    }
    
    func popToLoginView() {
        source?.navigationController?.popViewController(animated: true)
    }
    
}
