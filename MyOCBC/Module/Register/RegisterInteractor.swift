//
//  RegisterInteractor.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SwiftyJSON

final class RegisterInteractor: RegisterInteractorDelegate {
    
    weak var presenter: RegisterPresenterDelegate?
    
    init(presenter : RegisterPresenterDelegate?) {
        self.presenter = presenter
    }
    
    func postRegister(username: String, password: String) {
        let endpoint = Endpoint.Register.postRegister(username, password).build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.postRegisterResponse(response: success, error: failure)
        }
    }
    
    func postRegisterResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = RegisterModel(response)
            if error == nil {
                if model.status {
                    self.presenter?.postRegisterOnSuccess()
                } else {
                    self.presenter?.postRegisterOnFailure(message: model.error)
                }
            } else {
                self.presenter?.postRegisterOnFailure(message: model.error)
            }
        } else {
            self.presenter?.postRegisterOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
}
