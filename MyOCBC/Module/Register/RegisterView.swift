//
//  RegisterView.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import SnapKit

class RegisterView: UIView {
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let registerLbl = UILabel()
    let emailTF = TextFieldView(frame: CGRect(), title: "Username", placeholder: "Your username")
    let passwordTF = TextFieldView(frame: CGRect(), title: "Password", placeholder: "Enter password", isSecure: true)
    let confirmPasswordTF = TextFieldView(frame: CGRect(), title: "Confirm Password", placeholder: "Enter confirm password", isSecure: true)
    
    let confirmPasswordErrorLbl = UILabel()
    let registerBtn = UIButton()
    let loginBtn = UIButton()
    
    let successMessageView = SuccessFloatingView()
    let errorMessageView = ErrorFloatingView()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([scrollView])
        scrollView.addContentView(view: contentView)
        contentView.setupSubviews([registerLbl, emailTF, passwordTF, confirmPasswordTF, confirmPasswordErrorLbl, registerBtn, loginBtn, successMessageView, errorMessageView])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        scrollView.style {
            $0.bounces = false
            $0.showsVerticalScrollIndicator = false
        }
        
        contentView.style {
            $0.backgroundColor = .white
        }
        
        confirmPasswordErrorLbl.style {
            $0.textColor = .OCBC.errorTextRed
            $0.text = "Confirm password not matched"
            $0.isHidden = true
            $0.font = UIFont.MontserratSemiBold(ofSize: 12)
        }
        
        registerLbl.style {
            $0.text = "Register"
            $0.font = UIFont.MontserratBold(ofSize: 24)
            $0.textColor = .OCBC.mainTextColor
        }
        
        registerBtn.style {
            $0.backgroundColor = .OCBC.secondaryRed
            $0.setTitle("Register", for: .normal)
            $0.setTitleColor(.white, for: .normal)
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
            $0.titleLabel?.font = UIFont.MontserratBold(ofSize: 16)
        }
        
        loginBtn.style {
            $0.setTitle("Already have an account? Login", for: .normal)
            $0.setTitleColor(.OCBC.secondaryRed, for: .normal)
            $0.titleLabel?.font = UIFont.MontserratSemiBold(ofSize: 14)
        }
        
//        successMessageView.isHidden = true
        successMessageView.style {
            $0.isHidden = true
            $0.successLbl.text = "success creating account"
        }
        
        errorMessageView.isHidden = true
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalTo(self)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottomMargin)
        }
        
        registerLbl.snp.makeConstraints { make in
            make.top.equalTo(contentView).offset(140)
            make.leading.equalTo(contentView).offset(20)
        }
        
        emailTF.snp.makeConstraints { make in
            make.top.equalTo(registerLbl.snp.bottom).offset(35)
            make.leading.equalTo(registerLbl)
            make.trailing.equalTo(contentView).offset(-20)
        }
        
        passwordTF.snp.makeConstraints { make in
            make.top.equalTo(emailTF.snp.bottom).offset(25)
            make.leading.trailing.equalTo(emailTF)
        }
        
        confirmPasswordTF.snp.makeConstraints { make in
            make.top.equalTo(passwordTF.snp.bottom).offset(25)
            make.leading.trailing.equalTo(emailTF)
        }
        
        confirmPasswordErrorLbl.snp.makeConstraints { make in
            make.leading.equalTo(confirmPasswordTF)
            make.top.equalTo(confirmPasswordTF.snp.bottom).offset(5)
        }
        
        registerBtn.snp.makeConstraints { make in
            make.top.equalTo(confirmPasswordTF.snp.bottom).offset(25)
            make.height.equalTo(52)
            make.leading.trailing.equalTo(emailTF)
        }
        
        loginBtn.snp.makeConstraints { make in
            make.top.equalTo(registerBtn.snp.bottom).offset(14)
            make.leading.equalTo(registerBtn)
            make.bottom.equalTo(contentView)
        }
        
        successMessageView.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.trailing.equalTo(registerBtn)
            make.top.equalTo(loginBtn.snp.bottom).offset(24)
        }
        
        errorMessageView.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.trailing.equalTo(registerBtn)
            make.top.equalTo(loginBtn.snp.bottom).offset(24)
        }
    }
}
