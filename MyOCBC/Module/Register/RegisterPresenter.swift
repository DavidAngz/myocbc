//
//  RegisterPresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit

final class RegisterPresenter: RegisterPresenterDelegate {

    weak var view: RegisterViewDelegate?
    lazy var interactor: RegisterInteractorDelegate = RegisterInteractor(presenter: self)
    lazy var router: RegisterRouterDelegate? = RegisterRouter(view: view)
    
    init(view: RegisterViewDelegate?) {
        self.view = view
    }
    
    func popToLoginView() {
        router?.popToLoginView()
    }
    
    func postRegister(username: String, password: String) {
        interactor.postRegister(username: username, password: password)
    }
    
    func postRegisterOnSuccess() {
        view?.postRegisterOnSuccess()
    }
    
    func postRegisterOnFailure(message: String) {
        view?.postRegisterOnFailure(message: message)
    }
}
