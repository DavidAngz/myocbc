//
//  RegisterViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class RegisterViewController: UIViewController, RegisterViewDelegate {
    let root = RegisterView()
    lazy var presenter: RegisterPresenterDelegate = RegisterPresenter(view: self)
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        setRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func setRx() {
        root.loginBtn.rx.tap.subscribe(onNext: { _ in
            self.presenter.popToLoginView()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.confirmPasswordTF.textfield.rx.value.changed.subscribe(onNext: { newString in
            guard let newString = newString else {return}
            if self.root.confirmPasswordTF.textfield.text ?? "" + newString != self.root.passwordTF.textfield.text ?? "" && !(self.root.confirmPasswordTF.textfield.text?.isEmpty ?? true) {
                self.root.confirmPasswordErrorLbl.isHidden = false
            } else {
                self.root.confirmPasswordErrorLbl.isHidden = true
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.registerBtn.rx.tap.subscribe(onNext: { _ in
            let username = self.root.emailTF.textfield.text ?? ""
            let password = self.root.passwordTF.textfield.text ?? ""
            let confirmPassword = self.root.confirmPasswordTF.textfield.text ?? ""
            
            if confirmPassword != "" && password == confirmPassword {
                self.presentLoadingView(source: self)
                self.presenter.postRegister(username: username, password: password)
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
    
    func setTimer() {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.popToLoginPage), userInfo: nil, repeats: false)
    }
    
    @objc func popToLoginPage() {
        self.presenter.popToLoginView()
    }
}

extension RegisterViewController {
    func postRegisterOnSuccess() {
        dismissLoadingView(animated: false)
        root.errorMessageView.isHidden = true
        root.successMessageView.isHidden = false
        setTimer()
    }
    
    func postRegisterOnFailure(message: String) {
        dismissLoadingView(animated: false)
        
        if message.contains("username") {
            root.emailTF.errorState()
        } else if message.contains("password") {
            root.passwordTF.errorState()
        } else {
            root.emailTF.errorState()
            root.passwordTF.errorState()
        }
        
        root.errorMessageView.errorLbl.text = message
        root.errorMessageView.isHidden = false
    }
}
