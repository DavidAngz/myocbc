//
//  RegisterProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation

protocol RegisterViewDelegate: AnyObject {
    func postRegisterOnSuccess()
    func postRegisterOnFailure(message: String)
}

protocol RegisterPresenterDelegate: AnyObject {
    func popToLoginView()
    
    func postRegister(username: String, password: String)
    func postRegisterOnSuccess()
    func postRegisterOnFailure(message: String)
}

protocol RegisterInteractorDelegate: AnyObject {
    func postRegister(username: String, password: String)
}

protocol RegisterRouterDelegate: AnyObject {
    func popToLoginView()
}
