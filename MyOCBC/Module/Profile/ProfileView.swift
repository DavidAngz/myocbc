//
//  ProfileView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit

class ProfileView: UIView {
    let cardView = UIView()
    let cardLogoImgView = UIImageView()
    let cardNumberLbl = UILabel()
    let expiredLbl = UILabel()
    let expiredDateLbl = UILabel()
    
    let nameLbl = UILabel()
    let nameValueLbl = UILabel()
    
    let currentBalanceLbl = UILabel()
    let currentBalanceValueLbl = UILabel()
    
    let logoutBtn = UIButton()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([cardView, nameLbl, nameValueLbl, currentBalanceLbl, currentBalanceValueLbl, logoutBtn])
        cardView.setupSubviews([cardLogoImgView, cardNumberLbl, expiredLbl, expiredDateLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        cardView.style {
            $0.backgroundColor = .OCBC.secondaryRed
            $0.setLayer(cornerRadius: 16, clipToBounds: true)
        }
        
        cardLogoImgView.style {
            $0.image = UIImage(named: "ic_ocbc_logo_horizontal")
        }
        
        cardNumberLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 24)
            $0.textColor = .white
        }
        
        expiredLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 12)
            $0.textColor = .white
            $0.text = "Exp date"
        }
        
        expiredDateLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 16)
            $0.textColor = .white
            $0.text = "-"
        }
        
        nameLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 12)
            $0.textColor = .OCBC.mainTextColor
            $0.text = "Name"
        }
        
        nameValueLbl.style {
            $0.font = UIFont.MontserratBold(ofSize: 16)
            $0.textColor = .OCBC.mainRed
            $0.text = "David Angkasa"
        }
        
        currentBalanceLbl.style {
            $0.font = UIFont.MontserratMedium(ofSize: 12)
            $0.textColor = .OCBC.mainTextColor
            $0.text = "Current Balance"
        }
        
        currentBalanceValueLbl.style {
            $0.font = UIFont.MontserratBold(ofSize: 16)
            $0.textColor = .OCBC.mainRed
            $0.text = "$4,570.80"
        }
        
        logoutBtn.style {
            $0.setLayer(cornerRadius: 8, borderWidth: 2, borderColor: .OCBC.secondaryRed, clipToBounds: true)
            $0.setTitle("Log out", for: .normal)
            $0.setTitleColor(UIColor.OCBC.secondaryRed, for: .normal)
            $0.titleLabel?.font = UIFont.MontserratBold(ofSize: 16)
        }
    }
    
    func setupConstraints() {
        cardView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(60)
            make.leading.equalTo(self).offset(20)
            make.trailing.equalTo(self).offset(-20)
        }
        
        cardLogoImgView.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.width.equalTo(191)
            make.top.equalTo(cardView).offset(24)
            make.leading.equalTo(cardView).offset(20)
        }
        
        cardNumberLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardLogoImgView)
            make.top.equalTo(cardLogoImgView.snp.bottom).offset(24)
            make.trailing.equalTo(cardView).offset(-20)
        }
        
        expiredLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardLogoImgView)
            make.top.equalTo(cardNumberLbl.snp.bottom).offset(12)
        }
        
        expiredDateLbl.snp.makeConstraints { make in
            make.top.equalTo(expiredLbl.snp.bottom).offset(2)
            make.leading.equalTo(cardLogoImgView)
            make.bottom.equalTo(cardView).offset(-32)
        }
        
        nameLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardView)
            make.top.equalTo(cardView.snp.bottom).offset(32)
        }
        
        nameValueLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardView)
            make.top.equalTo(nameLbl.snp.bottom).offset(8)
            make.trailing.equalTo(self).offset(-20)
        }
        
        currentBalanceLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardView)
            make.top.equalTo(nameValueLbl.snp.bottom).offset(17)
            make.trailing.equalTo(self).offset(-20)
        }
        
        currentBalanceValueLbl.snp.makeConstraints { make in
            make.leading.equalTo(cardView)
            make.top.equalTo(currentBalanceLbl.snp.bottom).offset(8)
            make.trailing.equalTo(self).offset(-20)
        }
        
        logoutBtn.snp.makeConstraints { make in
            make.top.equalTo(currentBalanceValueLbl.snp.bottom).offset(32)
            make.height.equalTo(52)
            make.leading.equalTo(self).offset(20)
            make.trailing.equalTo(self).offset(-20)
        }
    }
}
