//
//  ProfilePresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

final class ProfilePresenter: ProfilePresenterDelegate {
    
    weak var view: ProfileViewDelegate?
    lazy var router: ProfileRouterDelegate? = ProfileRouter(view: view)
    
    init(view: ProfileViewDelegate?) {
        self.view = view
    }
    
    func popToLoginPage() {
        router?.popToLoginPage()
    }
    
}
