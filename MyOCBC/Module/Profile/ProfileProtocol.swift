//
//  ProfileProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation

protocol ProfileViewDelegate: AnyObject {
    
}

protocol ProfilePresenterDelegate: AnyObject {
    func popToLoginPage()
}

protocol ProfileRouterDelegate: AnyObject {
    func popToLoginPage()
}
