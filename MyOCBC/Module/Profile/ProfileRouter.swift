//
//  ProfileRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

final class ProfileRouter: ProfileRouterDelegate {

    weak var source: UIViewController?
    
    init(view: ProfileViewDelegate?) {
        source = view as? UIViewController
    }
    
    func popToLoginPage() {
        let vc = LoginViewController()
        Keychain.token.delete()
        source?.navigationController?.popToRootViewController(animated: true)
    }
    
}
