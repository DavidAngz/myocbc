//
//  ProfileViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class ProfileViewController: UIViewController, ProfileViewDelegate {
    let root = ProfileView()
    lazy var presenter: ProfilePresenterDelegate = ProfilePresenter(view: self)
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        setRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initialize()
    }
    
    func setRx() {
        root.logoutBtn.rx.tap.subscribe(onNext: { _ in
            self.showLogoutConfirmation()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
    
    func initialize() {
        root.cardNumberLbl.text = Keychain.cardNumber.load() ?? ""
        root.nameValueLbl.text = Keychain.name.load() ?? ""
        root.currentBalanceValueLbl.text = "SGD " + (Keychain.balance.load() ?? "") 
    }
    
    func showLogoutConfirmation() {
        let alert = UIAlertController(title: "Logout", message: "Are you sure to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Log Out", style: .default, handler: {(action) in
            self.presenter.popToLoginPage()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
