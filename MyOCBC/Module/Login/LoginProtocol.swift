//
//  LoginProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import Foundation

protocol LoginViewDelegate: AnyObject {
    func postLoginOnSuccess(data: LoginModel)
    func postLoginOnFailure(message: String)
}

protocol LoginPresenterDelegate: AnyObject {
    func pushToRegisterView()
    
    func postLogin(username: String, password: String)
    func postLoginOnSuccess(data: LoginModel)
    func postLoginOnFailure(message: String)
    
    func pushToHomeView()
}

protocol LoginInteractorDelegate: AnyObject {
    func postLogin(username: String, password: String)
}

protocol LoginRouterDelegate: AnyObject {
    func pushToRegisterView()
    func pushToHomeView()
}
