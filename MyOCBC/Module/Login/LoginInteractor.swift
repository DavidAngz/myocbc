//
//  LoginInteractor.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import SwiftyJSON
import Alamofire

final class LoginInteractor: LoginInteractorDelegate {

    weak var presenter: LoginPresenterDelegate?
    
    init(presenter : LoginPresenterDelegate?) {
        self.presenter = presenter
    }
    
    func postLogin(username: String, password: String) {
        let endpoint = Endpoint.Login.postLogin(username, password).build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.postLoginResponse(response: success, error: failure)
        }
    }
    
    func postLoginResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = LoginModel(response)
            if error == nil {
                if model.status {
                    Keychain.token.delete()
                    Keychain.token.save(data: model.token)
                    
                    Keychain.name.delete()
                    Keychain.name.save(data: model.username)
                    
                    Keychain.cardNumber.delete()
                    Keychain.cardNumber.save(data: model.accountNo)
                    self.presenter?.postLoginOnSuccess(data: model)
                } else {
                    self.presenter?.postLoginOnFailure(message: model.error)
                }
            } else {
                self.presenter?.postLoginOnFailure(message: model.error)
            }
        } else {
            self.presenter?.postLoginOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
}
