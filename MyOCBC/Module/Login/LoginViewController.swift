//
//  LoginViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController, LoginViewDelegate {
    
    let root = LoginView()
    lazy var presenter: LoginPresenterDelegate = LoginPresenter(view: self)
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super .viewDidLoad()
        view = root
        
        setRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func setRx() {
        root.registerBtn.rx.tap.subscribe(onNext: { _ in
            self.presenter.pushToRegisterView()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.loginBtn.rx.tap.subscribe(onNext: { _ in
            self.presentLoadingView(source: self)
            self.presenter.postLogin(username: self.root.emailTF.textfield.text ?? "", password: self.root.passwordTF.textfield.text ?? "")
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
}

extension LoginViewController {
    func postLoginOnSuccess(data: LoginModel) {
        dismissLoadingView(animated: false)
        presenter.pushToHomeView()
    }
    
    func postLoginOnFailure(message: String) {
        dismissLoadingView(animated: false)
        root.errorMessageView.isHidden = false
        root.errorMessageView.errorLbl.text = message
        root.emailTF.errorState()
        root.passwordTF.errorState()
    }
}
