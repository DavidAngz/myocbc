//
//  LoginRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit

final class LoginRouter: LoginRouterDelegate {

    weak var source: UIViewController?
    
    init(view: LoginViewDelegate?) {
        source = view as? UIViewController
    }
    
    func pushToRegisterView() {
        let registerView = RegisterViewController()
        source?.navigationController?.pushViewController(registerView, animated: true)
    }
    
    func pushToHomeView() {
        let mainMenu = NavigationMenuBaseController()
        source?.navigationController?.pushViewController(mainMenu, animated: true)
    }
}
