//
//  LoginPresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit

final class LoginPresenter: LoginPresenterDelegate {
    
    weak var view: LoginViewDelegate?
    lazy var interactor: LoginInteractorDelegate = LoginInteractor(presenter: self)
    lazy var router: LoginRouterDelegate? = LoginRouter(view: view)
    
    init(view: LoginViewDelegate?) {
        self.view = view
    }
    
    func pushToRegisterView() {
        router?.pushToRegisterView()
    }
    
    func postLogin(username: String, password: String) {
        interactor.postLogin(username: username, password: password)
    }
    
    func postLoginOnSuccess(data: LoginModel) {
        view?.postLoginOnSuccess(data: data)
    }
    
    func postLoginOnFailure(message: String) {
        view?.postLoginOnFailure(message: message)
    }
    
    func pushToHomeView() {
        router?.pushToHomeView()
    }
}
