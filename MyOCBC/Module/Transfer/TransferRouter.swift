//
//  TransferRouter.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

final class TransferRouter: TransferRouterDelegate {

    weak var source: UIViewController?
    
    init(view: TransferViewDelegate?) {
        source = view as? UIViewController
    }
    
}
