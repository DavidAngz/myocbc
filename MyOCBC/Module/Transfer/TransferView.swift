//
//  TransferView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit
import DropDown

class TransferView: UIView {
    let backBtn = UIButton()
    let sendMoneyLbl = UILabel()
    
    let recipientTF = TextFieldView(frame: CGRect(), title: "Recipient", placeholder: "Select Recipient")
    let recipientDropDown = DropDown()
    let recipientBtn = UIButton()
    let arrowDown = UIButton()
    let amountTF = TextFieldView(frame: CGRect(), title: "Amount", placeholder: "0.00")
    let currencyLbl = UILabel()
    let notesTF = TextFieldView(frame: CGRect(), title: "Notes", placeholder: "Enter notes")
    
    let sendBtn = UIButton()
    
    let successMessageView = SuccessFloatingView()
    let errorMessageView = ErrorFloatingView()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([backBtn, sendMoneyLbl, recipientTF, recipientDropDown,  arrowDown, recipientBtn, amountTF, currencyLbl, notesTF, sendBtn, successMessageView, errorMessageView])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backBtn.setImage(UIImage(named: "ic_arrow_left"), for: .normal)
        
        arrowDown.setImage(UIImage(named: "ic_arrow_down"), for: .normal)
        
        recipientTF.textfield.isUserInteractionEnabled = false
        amountTF.textfield.keyboardType = .numberPad
        
        currencyLbl.style {
            $0.text = "SGD"
            $0.textColor = UIColor.OCBC.lightGray
            $0.font = UIFont.MontserratSemiBold(ofSize: 16)
            $0.textAlignment = .right
        }
        
        sendMoneyLbl.style {
            $0.text = "Send Money"
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratBold(ofSize: 24)
        }
        
        sendBtn.style {
            $0.setTitle("Send Now", for: .normal)
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = UIFont.MontserratBold(ofSize: 16)
            $0.backgroundColor = .OCBC.secondaryRed
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
        }
        
        successMessageView.style {
            $0.isHidden = true
            $0.successLbl.text = "success sending money"
        }
        
        errorMessageView.isHidden = true
    }
    
    func setupConstraints() {
        backBtn.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(135)
            make.leading.equalTo(self).offset(20)
        }
        
        sendMoneyLbl.snp.makeConstraints { make in
            make.top.equalTo(backBtn.snp.bottom).offset(24)
            make.leading.equalTo(backBtn)
        }
        
        recipientTF.snp.makeConstraints { make in
            make.top.equalTo(sendMoneyLbl.snp.bottom).offset(27)
            make.leading.equalTo(backBtn)
            make.trailing.equalTo(self).offset(-20)
        }
        
        recipientBtn.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(recipientTF.mainView)
        }
        
        arrowDown.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.leading.equalTo(recipientTF.textfield.snp.trailing).offset(16)
            make.trailing.equalTo(recipientTF.mainView).offset(-14)
            make.centerY.equalTo(recipientTF.mainView)
        }
        
        amountTF.snp.makeConstraints { make in
            make.top.equalTo(recipientTF.snp.bottom).offset(16)
            make.leading.equalTo(backBtn)
            make.trailing.equalTo(self).offset(-20)
        }
        
        currencyLbl.snp.makeConstraints { make in
            make.width.equalTo(50)
            make.leading.equalTo(amountTF.textfield.snp.trailing).offset(16)
            make.trailing.equalTo(amountTF.mainView).offset(-14)
            make.centerY.equalTo(amountTF.mainView)
        }
        
        notesTF.snp.makeConstraints { make in
            make.top.equalTo(amountTF.snp.bottom).offset(16)
            make.leading.equalTo(backBtn)
            make.trailing.equalTo(self).offset(-20)
        }
        
        sendBtn.snp.makeConstraints { make in
            make.height.equalTo(52)
            make.top.equalTo(notesTF.snp.bottom).offset(24)
            make.leading.trailing.equalTo(recipientTF)
        }
        
        successMessageView.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.trailing.equalTo(sendBtn)
            make.top.equalTo(sendBtn.snp.bottom).offset(24)
        }
        
        errorMessageView.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.trailing.equalTo(sendBtn)
            make.top.equalTo(sendBtn.snp.bottom).offset(24)
        }
    }
}
