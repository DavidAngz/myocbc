//
//  TransferPresenter.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

final class TransferPresenter: TransferPresenterDelegate {
    
    
 
    weak var view: TransferViewDelegate?
    lazy var interactor: TransferInteractorDelegate = TransferInteractor(presenter: self)
    lazy var router: TransferRouterDelegate? = TransferRouter(view: view)
    
    init(view: TransferViewDelegate?) {
        self.view = view
    }
    
    func getContactList() {
        interactor.getContactList()
    }
    
    func getContactListOnSuccess(model: [ContactListModel]) {
        view?.getContactListOnSuccess(model: model)
    }
    
    func getContactListOnFailure(message: String) {
        view?.getContactListOnFailure(message: message)
    }
    
    func postSendMoney(accountNo: String, amount: Int, description: String) {
        interactor.postSendMoney(accountNo: accountNo, amount: amount, description: description)
    }
    
    func postSendMoneyOnSuccess() {
        view?.postSendMoneyOnSuccess()
    }
    
    func postSendMoneyOnFailure(message: String) {
        view?.postSendMoneyOnFailure(message: message)
    }
    
}
