//
//  TransferProtocol.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation

protocol TransferViewDelegate: AnyObject {
    func getContactListOnSuccess(model: [ContactListModel])
    func getContactListOnFailure(message: String)
    
    func postSendMoneyOnSuccess()
    func postSendMoneyOnFailure(message: String)
}

protocol TransferPresenterDelegate: AnyObject {
    func getContactList()
    func getContactListOnSuccess(model: [ContactListModel])
    func getContactListOnFailure(message: String)
    
    func postSendMoney(accountNo: String, amount: Int, description: String)
    func postSendMoneyOnSuccess()
    func postSendMoneyOnFailure(message: String)
}

protocol TransferInteractorDelegate: AnyObject {
    func getContactList()
    func postSendMoney(accountNo: String, amount: Int, description: String)
}

protocol TransferRouterDelegate: AnyObject {
    
}
