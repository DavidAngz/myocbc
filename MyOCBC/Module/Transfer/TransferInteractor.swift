//
//  TransferInteractor.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit
import SwiftyJSON
import Alamofire

final class TransferInteractor: TransferInteractorDelegate {

    weak var presenter: TransferPresenterDelegate?
    
    init(presenter : TransferPresenterDelegate?) {
        self.presenter = presenter
    }
    
    func getContactList() {
        let endpoint = Endpoint.Home.getContact.build()
        Network.shared().request(endpoint: endpoint) { success, failure in
            self.getContactListResponse(response: success, error: failure)
        }
    }
    
    func getContactListResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = BaseModel<ContactListModel>(response)
            if error == nil {
                if model.status {
                    self.presenter?.getContactListOnSuccess(model: model.dataArray)
                } else {
                    self.presenter?.getContactListOnFailure(message: model.error)
                }
            } else {
                self.presenter?.getContactListOnFailure(message: model.error)
            }
        } else {
            self.presenter?.getContactListOnFailure(message: error?.localizedDescription ?? "")
        }
    }
    
    func postSendMoney(accountNo: String, amount: Int, description: String) {
        let urlString = "https://green-thumb-64168.uc.r.appspot.com/transfer"
        guard let url = URL(string: urlString) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(Keychain.token.load() ?? "", forHTTPHeaderField: "Authorization")
        do {
            request.httpBody   = try JSONSerialization.data(withJSONObject: [
                "receipientAccountNo" : accountNo,
                "amount" : amount,
                "description" : description
            ])
        } catch let error {
            print("Error : \(error.localizedDescription)")
        }
        AF.request(request).responseJSON{ (response) in
            switch response.result {
            case .success(let value):
                self.postSendMoneyResponse(response: JSON(value), error: nil)
            case .failure(_):
                self.postSendMoneyResponse(response: nil, error: response.error)
            }
        }
    }
    
    func postSendMoneyResponse(response: JSON?, error: Error?) {
        if let response = response {
            let model = TransferModel(response)
            if error == nil {
                if model.status {
                    self.presenter?.postSendMoneyOnSuccess()
                } else {
                    self.presenter?.postSendMoneyOnFailure(message: model.error)
                }
            } else {
                self.presenter?.postSendMoneyOnFailure(message: model.error)
            }
        } else {
            self.presenter?.postSendMoneyOnFailure(message: error?.localizedDescription ?? "")
        }
    }
}
