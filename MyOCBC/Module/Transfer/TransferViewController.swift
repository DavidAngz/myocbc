//
//  TransferViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import RxSwift
import RxCocoa

class TransferViewController: UIViewController {
    let root = TransferView()
    var isQuickSend = false
    let disposeBag = DisposeBag()
    
    lazy var presenter: TransferPresenterDelegate = TransferPresenter(view: self)
    var model: [ContactListModel] = []
    var dropdownList: [String] = []
    
    var selectedAccountNo: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        setRx()
        
        presentLoadingView(source: self)
        presenter.getContactList()
    }
    
    func setRx() {
        root.backBtn.rx.tap.subscribe(onNext: { _ in
            self.navigationController?.popViewController(animated: true)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.recipientBtn.rx.tap.subscribe(onNext: { _ in
            self.root.recipientDropDown.direction = .bottom
            self.root.recipientDropDown.anchorView = self.root.recipientTF.textfield
            self.root.recipientDropDown.bottomOffset = CGPoint(x: 0, y:(self.root.recipientDropDown.anchorView?.plainView.bounds.height)!)
            self.root.recipientDropDown.dataSource = self.dropdownList
            self.root.recipientDropDown.show()
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        root.recipientDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.root.recipientTF.textfield.text = item
            self.selectedAccountNo = self.model[index].accountNo
        }
        
        root.sendBtn.rx.tap.subscribe(onNext: { _ in
            let amount = Int(self.root.amountTF.textfield.text ?? "0") ?? 0
            let description = self.root.notesTF.textfield.text ?? ""
            
            self.presentLoadingView(source: self)
            self.presenter.postSendMoney(accountNo: self.selectedAccountNo, amount: amount, description: description)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
}

extension TransferViewController: TransferViewDelegate {
    func getContactListOnSuccess(model: [ContactListModel]) {
        dismissLoadingView(animated: false)
        self.model = model
        for item in model {
            self.dropdownList.append(item.name)
        }
    }
    
    func getContactListOnFailure(message: String) {
        dismissLoadingView(animated: false)
    }
    
    func postSendMoneyOnSuccess() {
        dismissLoadingView(animated: false)
        
        root.successMessageView.isHidden = false
    }
    
    func postSendMoneyOnFailure(message: String) {
        dismissLoadingView(animated: false)
        root.errorMessageView.isHidden = false
        root.errorMessageView.errorLbl.text = message
    }
}
