//
//  TabItem.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
enum TabItem: String, CaseIterable {
    case home = "home"
    case transfer = "send"
    case profile = "profile"
    
    var viewController: UIViewController {
        switch self {
        case .home:
            return HomeViewController()
        case .transfer:
            return TransferViewController()
        case .profile:
            return ProfileViewController()
        }
    }
    
    var icon: UIImage {
        switch self {
        case .home:
            return UIImage(named: "ic_home")!
        case .transfer:
            return UIImage(named: "ic_transfer")!
        case .profile:
            return UIImage(named: "ic_profile_white")!
        }
    }
    
    var displayTitle: String {
        return self.rawValue.capitalized(with: nil)
    }

}
