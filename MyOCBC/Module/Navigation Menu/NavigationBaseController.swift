//
//  NavigationBaseController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//


import UIKit
import RxSwift
import RxCocoa

class NavigationMenuBaseController: UITabBarController {
//    var customTabBar: TabNavigationMenu!
    var customTabBar = NavigationBaseView()
    var tabBarHeight: CGFloat = 100.0
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTabBar()
        setRx()
    }
    
    func setRx() {
        customTabBar.transferBtn.rx.tap.subscribe(onNext: { _ in
            self.navigationController?.pushViewController(TransferViewController(), animated: true)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }
    
    func loadTabBar() {
            // We'll create and load our custom tab bar here
        let tabItems: [TabItem] = [.home, .transfer, .profile]
        
        self.setupCustomTabMenu(tabItems) { (controllers) in
            self.viewControllers = controllers
        }
        
        self.selectedIndex = 0 // default our selected index to the first item
    }
    
    func setupCustomTabMenu(_ menuItems: [TabItem], completion: @escaping ([UIViewController]) -> Void) {
    // handle creation of the tab bar and attach touch event listeners
        let frame = tabBar.frame
        var controllers = [UIViewController]()
        // hide the tab bar
        tabBar.isHidden = true
        self.customTabBar = NavigationBaseView(menuItems: menuItems, frame: frame)
        self.customTabBar.translatesAutoresizingMaskIntoConstraints = false
        self.customTabBar.clipsToBounds = true
        self.customTabBar.itemTapped = self.changeTab
        // Add it to the view
        self.view.addSubview(customTabBar)
        // Add positioning constraints to place the nav menu right where the tab bar should be
        self.customTabBar.snp.makeConstraints { (make) in
            make.leading.trailing.width.bottom.equalTo(tabBar)
//            make.height.equalTo(tabBarHeight)
        }
        for i in 0 ..< menuItems.count {
            controllers.append(menuItems[i].viewController) // we fetch the matching view controller and append here
        }
        self.view.layoutIfNeeded() // important step
        completion(controllers) // setup complete. handoff here
    }
    
    func changeTab(tab: Int) {
        self.selectedIndex = tab
    }
}
