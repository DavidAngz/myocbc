//
//  TabNavigationMenu.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit
class TabNavigationMenu: UIView {
    
    let mainView = UIView()
    
    var itemTapped: ((_ tab: Int) -> Void)?
    var activeItem: Int = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(menuItems: [TabItem], frame: CGRect) {
        self.init(frame: frame)
        // Convenience init body
        self.addSubview(mainView)
        mainView.backgroundColor = UIColor.clear
        mainView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(self)
        }
        
        self.layer.backgroundColor = UIColor.white.cgColor
        for i in 0 ..< menuItems.count {
            let itemWidth = self.frame.width / CGFloat(menuItems.count)
            let leadingAnchor = itemWidth * CGFloat(i)
            
            var itemView = UIView()
            if i == 1 {
                itemView = self.addTransfer()
                mainView.addSubview(itemView)
                itemView.snp.makeConstraints { (make) in
                    make.top.equalTo(self)
                    make.leading.equalTo(mainView).offset(leadingAnchor)
                    make.centerY.equalTo(mainView.snp.top)
                    make.bottom.equalTo(mainView)
                }
            } else {
                itemView = self.createTabItem(item: menuItems[i])
                mainView.addSubview(itemView)
                itemView.snp.makeConstraints { (make) in
                    //                    make.top.height.equalTo(self)
                    make.top.equalTo(mainView)
                    make.leading.equalTo(mainView).offset(leadingAnchor)
                    make.bottom.equalTo(mainView)
                }
            }
            
            itemView.translatesAutoresizingMaskIntoConstraints = false
            itemView.clipsToBounds = true
            itemView.tag = i
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.activateTab(tab: 0) // activate the first tab
    }
    
    func createTabItem(item: TabItem) -> UIView {
        let tabBarItem = UIView(frame: CGRect.zero)
        let itemTitleLabel = UILabel(frame: CGRect.zero)
        let itemIconView = UIImageView(frame: CGRect.zero)
        itemTitleLabel.text = item.displayTitle
        itemTitleLabel.textAlignment = .center
        itemTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        itemTitleLabel.clipsToBounds = true
        
        itemIconView.image = item.icon.withRenderingMode(.automatic)
        itemIconView.translatesAutoresizingMaskIntoConstraints = false
        itemIconView.clipsToBounds = true
        tabBarItem.layer.backgroundColor = UIColor.white.cgColor
        tabBarItem.addSubview(itemIconView)
        tabBarItem.addSubview(itemTitleLabel)
        tabBarItem.translatesAutoresizingMaskIntoConstraints = false
        tabBarItem.clipsToBounds = true
        itemIconView.snp.makeConstraints { (make) in
            make.size.equalTo(25)
            make.centerX.equalTo(tabBarItem)
            make.top.equalTo(tabBarItem).offset(8)
        }
        
        itemTitleLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(tabBarItem)
            make.top.equalTo(itemIconView.snp.bottom).offset(4)
            make.bottom.equalTo(tabBarItem)
        }
        tabBarItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap))) // Each item should be able to trigger and action on tap
        return tabBarItem
    }

    
    func addTransfer() -> UIView {
        let transferView = UIView(frame: CGRect.zero)
        let transferImg = UIImageView()
        transferImg.image = UIImage(named: "ic_transfer")
        transferImg.setLayer(cornerRadius: 20)
        transferView.addSubview(transferImg)
        transferImg.snp.makeConstraints { (make) in
            make.top.leading.equalTo(transferView).offset(10)
            make.trailing.bottom.equalTo(transferView).offset(-10)
            make.size.equalTo(40)
        }
        transferView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
        return transferView
    }

    @objc func handleTap(_ sender: UIGestureRecognizer) {
        self.switchTab(from: self.activeItem, to: sender.view!.tag)
    }
    func switchTab(from: Int, to: Int) {
        self.deactivateTab(tab: from)
        self.activateTab(tab: to)
    }
    func activateTab(tab: Int) {
        let tabToActivate = self.subviews[tab]
        let borderWidth = tabToActivate.frame.size.width - 20
        let borderLayer = CALayer()
        borderLayer.backgroundColor = UIColor.green.cgColor
        borderLayer.name = "active border"
        borderLayer.frame = CGRect(x: 10, y: 0, width: borderWidth, height: 2)
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, delay: 0.0, options: [.curveEaseIn, .allowUserInteraction], animations: {
                tabToActivate.layer.addSublayer(borderLayer)
                tabToActivate.setNeedsLayout()
                tabToActivate.layoutIfNeeded()
            })
            self.itemTapped?(tab)
        }
        self.activeItem = tab
    }
    func deactivateTab(tab: Int) {
        let inactiveTab = self.subviews[tab]
        let layersToRemove = inactiveTab.layer.sublayers!.filter({ $0.name == "active border" })
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveEaseIn, .allowUserInteraction], animations: {
                layersToRemove.forEach({ $0.removeFromSuperlayer() })
                inactiveTab.setNeedsLayout()
                inactiveTab.layoutIfNeeded()
            })
        }
    }

}
