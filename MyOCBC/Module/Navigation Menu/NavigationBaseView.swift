//
//  NavigationBaseView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit

class NavigationBaseView: UIView {

    let mainView = UIView()
    let transferView = UIView()
    let transferImgView = UIImageView()
    let transferLbl = UILabel()
    let transferBtn = UIButton()
    
    let homeView = IconTitleView(frame: CGRect.zero, img: "ic_home_white", title: "Home", color: .OCBC.lightGray)
    let rightView = UIView()
    let profileView = IconTitleView(frame: CGRect.zero, img: "ic_profile_white", title: "Profile", color: .OCBC.lightGray)
    
    var itemTapped: ((_ tab: Int) -> Void)?
    var activeItem: Int = 0
    
    var menuItems: [TabItem] = []

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        setupSubviews([mainView, transferView, transferImgView, transferLbl, transferBtn])
        mainView.setupSubviews([homeView, rightView, profileView])
        setupConstraints()
        setupView()
    }
    
    convenience init(menuItems: [TabItem], frame: CGRect) {
        self.init(frame: frame)
        self.activateTab(tab: 0)
        self.menuItems = menuItems
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func handleTap(_ sender: UIGestureRecognizer) {
        self.switchTab(from: self.activeItem, to: sender.view!.tag)
    }
    
    func switchTab(from: Int, to: Int) {
        self.deactivateTab(tab: from)
        self.activateTab(tab: to)
    }
    
    func activateTab(tab: Int) {
        self.itemTapped?(tab)
        self.activeItem = tab
        changeIcon(tab: tab, isHide: false)
    }
    
    func deactivateTab(tab: Int) {
        changeIcon(tab: tab, isHide: true)
    }
    
    func changeIcon(tab: Int, isHide: Bool) {
        
        var img: String = ""
        if tab == 0 {
            img = "ic_home"
        } else if tab == 2 {
            img = "ic_profile"
        }
        
        if isHide {
            img.append("_white")
        }
        
        if let iconView = viewWithTag(tab) as? IconTitleView, img != "" {
            iconView.iconImg.image = UIImage(named: img)
            iconView.titleLbl.textColor = isHide ? .OCBC.lightGray : .OCBC.secondaryRed
            iconView.titleLbl.font = isHide ? UIFont.MontserratSemiBold(ofSize: 12) : UIFont.MontserratBold(ofSize: 12)
        } else if tab == 0 {
            homeView.iconImg.image = UIImage(named: img)
            homeView.titleLbl.textColor = isHide ? .OCBC.lightGray : .OCBC.secondaryRed
            homeView.titleLbl.font = isHide ? UIFont.MontserratSemiBold(ofSize: 12) : UIFont.MontserratBold(ofSize: 12)
        } else if tab == 1 {
            transferLbl.textColor = isHide ? .OCBC.lightGray : .OCBC.secondaryRed
            transferLbl.font = isHide ? UIFont.MontserratSemiBold(ofSize: 12) : UIFont.MontserratBold(ofSize: 12)
        }
    }

    func setupView() {
        mainView.style {
            $0.addCorners(radius: 20, corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
            $0.backgroundColor = .white
            $0.layer.shadowOffset = CGSize(width: 0,
                                              height: -3)
            $0.layer.shadowRadius = 3
            $0.layer.shadowOpacity = 0.2
            $0.layer.shadowColor = UIColor.OCBC.lightGray.cgColor
            $0.layer.masksToBounds = false
        }
        
        homeView.style {
            $0.tag = 0
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
        }
        //        homeView.isEnabled = false
        
        transferView.style {
            $0.backgroundColor = .OCBC.secondaryRed
            $0.setLayer(cornerRadius: self.transferView.frame.height / 2, clipToBounds: true)
            $0.tag = 1
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
        }
                
        transferImgView.image = UIImage(named: "ic_transfer")
        
        transferLbl.style {
            $0.text = "Send"
            $0.font = UIFont.MontserratSemiBold(ofSize: 12)
            $0.textColor = .OCBC.lightGray
        }

        profileView.style {
            $0.tag = 2
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
        }

    }
    
    func setupConstraints() {
        mainView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(self)
            make.height.equalTo(100)
        }
        
        homeView.snp.makeConstraints { (make) in
//            make.centerY.equalTo(mainView)
            make.bottom.equalTo(safeAreaLayoutGuide)
            make.leading.equalTo(mainView)
            make.width.equalTo(UIScreen.main.bounds.width / 2 - 32)
        }

        transferView.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.centerY.equalTo(mainView.snp.top).offset(5)
            make.centerX.equalTo(self)
            make.leading.equalTo(homeView.snp.trailing)
            make.size.equalTo(64)
        }

        transferImgView.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.center.equalTo(transferView)
        }
        
        transferLbl.snp.makeConstraints { make in
            make.centerX.equalTo(transferView)
            make.top.equalTo(transferView.snp.bottom).offset(8)
        }
        
        transferBtn.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(transferView)
        }
        
        rightView.snp.makeConstraints { make in
            make.trailing.equalTo(mainView)
            make.width.equalTo(UIScreen.main.bounds.width / 2 - 32)
        }
        
        profileView.snp.makeConstraints { make in
            make.bottom.equalTo(safeAreaLayoutGuide)
            make.trailing.equalTo(mainView)
//            make.leading.equalTo(transferView.snp.trailing)
            make.width.equalTo(UIScreen.main.bounds.width / 2 - 32)
//            make.centerX.equalTo(rightView)
        }

        setNeedsLayout()
        layoutIfNeeded()
    }
}
