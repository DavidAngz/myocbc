//
//  IconTitleView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit

class IconTitleView: UIView {
    let iconImg = UIImageView()
    let titleLbl = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: mainScreen)
        setupSubviews([iconImg, titleLbl])
        
        setupConstraints()
        setupView()
    }
    
    convenience init(frame: CGRect, img: String, title: String, color: UIColor = .white) {
        self.init(frame: frame)
        iconImg.image = UIImage(named: img)
        iconImg.tintColor = color
        
        titleLbl.text = title
        titleLbl.textColor = color
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        iconImg.contentMode = .scaleAspectFit
        iconImg.isUserInteractionEnabled = true
        
        titleLbl.font = UIFont.MontserratBold(ofSize: 12)
    }
    
    func setupConstraints() {
        
        iconImg.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(self)
            make.size.equalTo(24)
        }
        
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(iconImg.snp.bottom).offset(4)
            make.centerX.equalTo(self)
            make.bottom.equalTo(self)
        }
        
        setNeedsLayout()
        layoutIfNeeded()
    }
}
