//
//  ErrorFloatingView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit

class ErrorFloatingView: UIView {
    let mainView = UIView()
    let errorLbl = UILabel()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([mainView, errorLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        mainView.style {
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
            $0.backgroundColor = .OCBC.errorBackgroundRed
        }
        
        errorLbl.style {
            $0.font = UIFont.MontserratSemiBold(ofSize: 12)
            $0.textColor = .OCBC.errorTextRed
        }
    }
    
    func setupConstraints() {
        mainView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }
        
        errorLbl.snp.makeConstraints { make in
            make.leading.equalTo(mainView).offset(20)
            make.centerY.equalTo(mainView)
            make.trailing.equalTo(mainView).offset(-20)
        }
    }
}
