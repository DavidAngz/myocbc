//
//  TextFieldView.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class TextFieldView: UIView, UITextFieldDelegate {
    let titleLbl = UILabel()
    let mainView = UIView()
    let textfield = UITextField()
    let showPasswordBtn = ShowPasswordBtn()
    let errorMsgLbl = UILabel()
    
    let disposable = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: mainScreen)
        
        setupSubviews([titleLbl, mainView, errorMsgLbl])
        mainView.setupSubviews([textfield, showPasswordBtn])
        
        textfield.delegate = self
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(frame: CGRect, title: String, placeholder: String, isSecure: Bool = false) {
        self.init(frame: frame)
        titleLbl.text = title
        
        textfield.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.OCBC.lightGray,
                                                                          NSAttributedString.Key.font : UIFont.MontserratSemiBold(ofSize: 16)])
        
        textfield.isSecureTextEntry = isSecure
        
        if isSecure {
            showPasswordBtn.isHidden = false
            showPasswordBtn.rx.tap.subscribe(onNext: { _ in
                if self.textfield.isSecureTextEntry == false {
                    self.textfield.isSecureTextEntry = true
                } else {
                    self.textfield.isSecureTextEntry = false
                }
            }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposable)
        }
    }
    
    func errorState() {
        mainView.style {
            $0.setLayer(cornerRadius: 8, borderWidth: 2, borderColor: .OCBC.errorTextRed, clipToBounds: true)
        }
    }
    
    func setupView() {
        titleLbl.style {
            $0.font = UIFont.MontserratSemiBold(ofSize: 12)
            $0.textColor = .OCBC.mainTextColor
        }
        
        textfield.style {
            $0.font = UIFont.MontserratMedium(ofSize: 16)
            $0.textColor = .OCBC.mainTextColor
            $0.autocapitalizationType = .none
            $0.textContentType = .oneTimeCode
            $0.autocorrectionType = .no
        }
        
        mainView.style {
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
            $0.backgroundColor = .OCBC.pink
        }
        
        showPasswordBtn.style {
            $0.isHidden = true
        }
    }
    
    func setupConstraints() {
        titleLbl.snp.makeConstraints { make in
            make.top.leading.equalTo(self)
        }
        
        mainView.snp.makeConstraints { make in
            make.top.equalTo(titleLbl.snp.bottom).offset(8)
            make.leading.trailing.bottom.equalTo(self)
            make.height.equalTo(52)
        }
        
        textfield.snp.makeConstraints { make in
            make.top.bottom.equalTo(mainView)
            make.leading.equalTo(mainView).offset(16)
            make.trailing.equalTo(showPasswordBtn.snp.leading).offset(-5)
        }
        
        showPasswordBtn.snp.makeConstraints { make in
            make.centerY.equalTo(mainView)
            make.trailing.equalTo(mainView).offset(-20)
            make.size.equalTo(21)
        }
    }
}

class ShowPasswordBtn : UIButton {
    let imgView = UIImageView()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        self.setupSubviews([imgView])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        imgView.style {
            $0.image = UIImage(named: "ic_show_password")?.withTintColor(UIColor.white.alpha(0.24))
        }
    }
    
    func setupConstraints() {
        imgView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }
    }
}
