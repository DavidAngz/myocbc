//
//  LoadingView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import Lottie
import SnapKit

class LoadingView : UIView {
    let animationView = AnimationView(name: "loadingIndicator")
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        self.backgroundColor = UIColor.black.alpha(0.5)
        
        setupSubviews([animationView])
        
//        setupConstraints()
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        animationView.style {
//            $0.contentMode = .scaleAspectFit
            $0.loopMode = .loop
            $0.animationSpeed = 2
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func setupConstraints() {
        animationView.snp.makeConstraints { (make) in
            make.height.equalTo(200)
            make.width.equalTo(250)
            make.centerX.centerY.equalTo(self)
        }
    }
}
