//
//  SuccessFloatingView.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import UIKit
import SnapKit

class SuccessFloatingView: UIView {
    let mainView = UIView()
    let successLbl = UILabel()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([mainView, successLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        mainView.style {
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
            $0.backgroundColor = .OCBC.successBackgroundGreen
        }
        
        successLbl.style {
            $0.font = UIFont.MontserratSemiBold(ofSize: 12)
            $0.textColor = .OCBC.successTextGreen
        }
    }
    
    func setupConstraints() {
        mainView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }
        
        successLbl.snp.makeConstraints { make in
            make.leading.equalTo(mainView).offset(20)
            make.centerY.equalTo(mainView)
            make.trailing.equalTo(mainView).offset(-20)
        }
    }
}
