//
//  BaseModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation
import SwiftyJSON

protocol BaseModelProtocol {
    init(_ json : JSON)
}

class BaseModel<T : BaseModelProtocol> : BaseModelProtocol {
    
    var status: Bool = true
    var error: String = ""
    var total: Int = 0
    var data: T?
    var dataArray: [T] = []
    
    required init(_ json: JSON) {
        
        if json["status"].string == "success" {
            status = true
        } else {
            status = false
        }
        
        if json["data"].dictionary != nil {
            self.data = T(json["data"])
        } else {
            self.data = T(json)
        }
        
        if let data = json["data"].array {
            for item in data {
                dataArray.append(T(item))
            }
        }
        
        if json["error"].string != nil {
            self.error = json["error"].string ?? ""
        }
    }
    
}
