//
//  RegisterModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation
import SwiftyJSON

struct RegisterModel: BaseModelProtocol {
    var status: Bool = false
    var error: String = ""
    
    init(_ json: JSON) {
        if json["status"].string == "success" {
            status = true
        } else {
            status = false
        }
        
        error = json["error"].string ?? ""
    }
}
