//
//  LoginModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation
import SwiftyJSON

struct LoginModel: BaseModelProtocol {
    var status: Bool = false
    var token: String = ""
    var username: String = ""
    var accountNo: String = ""
    var error: String = ""
    
    init(_ json: JSON) {
        if json["status"].string == "success" {
            status = true
        } else {
            status = false
        }
//        status = json["status"].bool ?? false
        token = json["token"].string ?? ""
        username = json["username"].string ?? ""
        accountNo = json["accountNo"].string ?? ""
        error = json["error"].string ?? ""
    }
}
