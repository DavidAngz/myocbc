//
//  TransactionModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation
import SwiftyJSON

struct TransactionModel: BaseModelProtocol {
    var transactionId: String = ""
    var amount: Int = 0
    var transactionDate: String = ""
    var description: String = ""
    var transactionType: String = ""
    var receipientName: String = ""
    var receipientAccountNo: String = ""
    
    init(_ json: JSON) {
        transactionId = json["transactionId"].string ?? ""
        amount = json["amount"].int ?? 0
        transactionDate = json["transactionDate"].string ?? ""
        description = json["description"].string ?? ""
        transactionType = json["transactionType"].string ?? ""
        
        if let receipient = json["receipient"].dictionary {
            receipientName = receipient["accountHolder"]?.string ?? ""
            receipientAccountNo = receipient["accountNo"]?.string ?? ""
        }
    }
}
