//
//  BalanceModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation
import SwiftyJSON

struct BalanceModel: BaseModelProtocol {
    var status: Bool = false
    var accountNo: String = ""
    var balance: Int = 0
    var error: String = ""
    
    init(_ json: JSON) {
        if json["status"].string == "success" {
            status = true
        } else {
            status = false
        }
        
        accountNo = json["accountNo"].string ?? ""
        balance = json["balance"].int ?? 0
        error = json["error"].string ?? ""
    }
}
