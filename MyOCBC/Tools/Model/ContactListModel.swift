//
//  ContactModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation
import SwiftyJSON

struct ContactListModel: BaseModelProtocol {
    var id: String = ""
    var name: String = ""
    var accountNo: String = ""
    
    init(_ json: JSON) {
        id = json["id"].string ?? ""
        name = json["name"].string ?? ""
        accountNo = json["accountNo"].string ?? ""
    }
}
