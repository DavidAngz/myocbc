//
//  TransferModel.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import Foundation
import SwiftyJSON

struct TransferModel: BaseModelProtocol {
    var status: Bool = false
    var error: String = ""
    
    init(_ json: JSON) {
        if json["status"].string == "success" {
            status = true
        } else {
            status = false
        }
        
        error = json["error"].string ?? ""
    }
}
