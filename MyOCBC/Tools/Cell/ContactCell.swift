//
//  ContactCell.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit
import SnapKit
import SkeletonView

class ContactCell: UICollectionViewCell {
    let mainView = UIView()
    let profileImg = UIImageView()
    let nameLbl = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: mainScreen)
        self.isSkeletonable = true
        
        contentView.setupSubviews([mainView])
        mainView.setupSubviews([profileImg, nameLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(model: ContactListModel) {
        nameLbl.text = model.name
    }
    
    func setupView() {
        contentView.isSkeletonable = true
        
        mainView.style {
            $0.backgroundColor = .OCBC.pink
            $0.setLayer(cornerRadius: 8, clipToBounds: true)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        profileImg.style {
            $0.image = UIImage(named: "ic_default_profile")
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = Float(self.profileImg.frame.height / 2)
        }
        
        nameLbl.style {
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratMedium(ofSize: 12)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
    }
    
    func setupConstraints() {
        mainView.snp.makeConstraints { make in
            make.top.leading.equalTo(contentView)
            make.trailing.bottom.equalTo(contentView)
        }
        
        profileImg.snp.makeConstraints { make in
            make.size.equalTo(32)
            make.centerY.equalTo(mainView)
            make.leading.equalTo(mainView).offset(12)
        }
        
        nameLbl.snp.makeConstraints { make in
            make.centerY.equalTo(profileImg)
            make.leading.equalTo(profileImg.snp.trailing).offset(8)
            make.trailing.equalTo(mainView).offset(-8)
        }
    }
}
