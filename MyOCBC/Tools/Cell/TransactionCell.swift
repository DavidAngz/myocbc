//
//  TransactionCell.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit
import SnapKit
import SkeletonView

class TransactionCell: UICollectionViewCell {
    let mainView = UIView()
    let dateLbl = UILabel()
    let nameLbl = UILabel()
    let accountNoLbl = UILabel()
    let moneyLbl = UILabel()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        self.isSkeletonable = true
        
        contentView.setupSubviews([mainView])
        mainView.setupSubviews([dateLbl, nameLbl, accountNoLbl, moneyLbl])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(model: TransactionModel) {
        dateLbl.text = model.transactionDate
        nameLbl.text = model.receipientName
        accountNoLbl.text = model.receipientAccountNo
        
        if model.transactionType == "transfer" {
            moneyLbl.textColor = .OCBC.secondaryRed
            moneyLbl.text = "-SGD\(model.amount)"
        } else {
            moneyLbl.textColor = .OCBC.mainTextColor
            moneyLbl.text = "SGD\(model.amount)"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd, HH:mm"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let date = dateFormatter.date(from: model.transactionDate)
        
        dateLbl.text = dateFormatter.string(from: date ?? Date())
    }
    
    func setupView() {
        contentView.isSkeletonable = true
        
        mainView.style {
            $0.setLayer(cornerRadius: 8, borderWidth: 0.5, borderColor: .OCBC.lightGray, clipToBounds: true)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        dateLbl.style {
            $0.textColor = .OCBC.lightGray
            $0.font = UIFont.MontserratMedium(ofSize: 12)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        nameLbl.style {
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratMedium(ofSize: 16)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        accountNoLbl.style {
            $0.textColor = .OCBC.lightGray
            $0.font = UIFont.MontserratMedium(ofSize: 14)
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
        
        moneyLbl.style {
            $0.textColor = .OCBC.mainTextColor
            $0.font = UIFont.MontserratMedium(ofSize: 16)
            $0.textAlignment = .right
            $0.isSkeletonable = true
            $0.skeletonCornerRadius = 8
        }
    }
    
    func setupConstraints() {
        mainView.snp.makeConstraints { make in
            make.leading.equalTo(contentView).offset(20)
            make.trailing.equalTo(contentView).offset(-20)
            make.top.bottom.equalTo(contentView)
        }
        
        dateLbl.snp.makeConstraints { make in
            make.top.equalTo(mainView).offset(16)
            make.leading.equalTo(mainView).offset(20)
        }
        
        nameLbl.snp.makeConstraints { make in
            make.top.equalTo(dateLbl.snp.bottom).offset(8)
            make.leading.equalTo(dateLbl)
            make.trailing.equalTo(moneyLbl.snp.leading).offset(-5)
        }
        
        accountNoLbl.snp.makeConstraints { make in
            make.top.equalTo(nameLbl.snp.bottom).offset(8)
            make.leading.equalTo(dateLbl)
        }
        
        moneyLbl.snp.makeConstraints { make in
            make.centerY.equalTo(mainView)
            make.trailing.equalTo(mainView).offset(-45)
        }
    }
}
