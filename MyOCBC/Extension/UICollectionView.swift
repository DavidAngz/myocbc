//
//  UICollectionView.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

extension UICollectionView {
    func updateHeight(numberOfItemsInRow totalItem: CGFloat = 1, totalData: Int) {
        let total = CGFloat(totalData)
        let layout = (collectionViewLayout as! UICollectionViewFlowLayout)
        let height = layout.itemSize.height
        let totaldata = total / totalItem
        snp.updateConstraints { (make) in
            let value = Double(totaldata).rounded(.up)
            var items = CGFloat(value) * height
            items += layout.sectionInset.top + layout.sectionInset.bottom
            items += (layout.minimumLineSpacing * (totaldata - 1))
            make.height.equalTo(items)
        }
    }
}
