//
//  UIFont.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit

extension UIFont {
    class func MontserratMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: size)!
    }
    
    class func MontserratSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-SemiBold", size: size)!
    }
    
    class func MontserratBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: size)!
    }
}
