//
//  UIViewController.swift
//  MyOCBC
//
//  Created by David Angkasa on 17/07/22.
//

import Foundation
import UIKit

extension UIViewController {
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
    func presentLoadingView(source: UIViewController) {
        let vc = LoadingViewController()
        
        vc.modalPresentationStyle = .overCurrentContext
        source.present(vc, animated: false, completion: nil)
    }
    
    func dismissLoadingView(animated: Bool, _ completion: (() -> Void)? = nil) {
        if (getCurrentViewController() as? LoadingViewController) != nil {
            dismiss(animated: animated, completion: completion)
        } else {
            print(getCurrentViewController())
            print("dont have uialertviewcontroller")
        }
    }
}
