//
//  UIColor.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit

extension UIColor {
    
    public convenience init(hexa: Int) {
        let mask = 0xFF
        let limit: CGFloat = 255.0
        let red = CGFloat((hexa >> 16) & mask) / limit
        let green = CGFloat((hexa >> 8) & mask) / limit
        let blue = CGFloat(hexa & mask) / limit
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    public convenience init(hexa: String) {
        var colorString = hexa.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        colorString = colorString.filter { (string) -> Bool in
            if let ascii = string.asciiValue {
            if ascii <= Int8(57) && ascii >= Int8(48) {
                return true
            } else if ascii <= 90 && ascii >= 65 {
                return true
            }
            return false
            }
            return false
        }
        
        if colorString.count != 6 {
            self.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        }
        
        var rgbHex: UInt32 = 0
        Scanner(string: colorString).scanHexInt32(&rgbHex)
        let limit: CGFloat = 255.0
        let red = CGFloat((rgbHex & 0xFF0000) >> 16)  / limit
        let green = CGFloat((rgbHex & 0x00FF00) >> 8) / limit
        let blue = CGFloat(rgbHex & 0x0000FF) / limit
        self.init(red: red, green: green, blue: blue, alpha: 1)
        
    }
    
    public func alpha(_ value: CGFloat) -> UIColor {
        return self.withAlphaComponent(value)
    }
    
    open class var primary: UIColor {
        return UIColor.clear
    }
    
    open class var secondary: UIColor {
        return UIColor.clear
    }
    
    open class var background: UIColor {
        return UIColor.init(hexa: 0x1D1724)
    }
    
    open class var backgroundAlpha: UIColor {
        // like fader but used for background
        return UIColor.grayAlpha.alpha(0.4)
    }
    
    open class var firstGray: UIColor {
        // first dark gray
        return UIColor.init(hexa: 0x353535)
    }
    
    open class var secondGray: UIColor {
        // second dark gray
        return UIColor.init(hexa: 0x555555)
    }
    
    open class var thirdGray: UIColor {
        // first light gray
        return UIColor.init(hexa: 0x757575)
    }
    
    open class var lastGray: UIColor {
        // second light gray
        return UIColor.init(hexa: 0xA5A5A5)
    }
    
    open class var silverGray: UIColor {
        // pure silver color
        return UIColor.init(hexa: 0xFAFAFA)
    }
    
    open class var grayAlpha: UIColor {
        // like fader
        return UIColor.init(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5)
    }
    
    open class var grayIcon: UIColor {
        return UIColor.init(hexa: 0xD5D5D5)
    }
    
    open class var blackAlpha: UIColor {
        return UIColor.init(hexa: 0xB20000)
    }
    
    open class var textGray: UIColor {
        // pure silver color
        return UIColor.init(hexa: 0x616161)
    }
    
    open class var backgroundGray: UIColor {
        // pure silver color
        return UIColor.init(hexa: 0xEEEEEE)
    }
    
    open class var textSecondGray: UIColor {
        // pure silver color
        return UIColor.init(hexa: 0xBDBDBD)
    }
    
    open class var opaqueDarkGray: UIColor {
        return UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.9)
    }
    
    open class var opaqueLightGray: UIColor {
        return UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 0.6)
    }
    
    open class var blue300: UIColor {
        return #colorLiteral(red: 0.3098039216, green: 0.7647058824, blue: 0.968627451, alpha: 1) //4FC3F7
    }
    
    open class var bluesky: UIColor {
        return UIColor.init(hexa: 0xB9E2F5)
    }
    
    open class var golden: UIColor {
        return UIColor.init(hexa: 0xF9A605)
    }
    
    open class var shimmerGray: UIColor {
        return UIColor.init(hexa: 0xF2F2F2)
    }
    
    open class var appleBlue: UIColor {
        return UIColor.init(hexa: 0x2A9DF4)
    }
    
    open class var appleGreen: UIColor {
        return UIColor.init(hexa: 0x2CB742)
    }
    
    public class OCBC {
        open class var pink: UIColor {
            return UIColor.init(hexa: 0xFFF5F5)
        }
        
        open class var mainTextColor: UIColor {
            return UIColor.init(hexa: 0x160407)
        }
        
        open class var lightGray: UIColor {
            return UIColor.init(hexa: 0x999999)
        }
        
        open class var mainRed: UIColor {
            return UIColor.init(hexa: 0x641220)
        }
        
        open class var secondaryRed: UIColor {
            return UIColor.init(hexa: 0xB21E35)
        }
        
        open class var successBackgroundGreen: UIColor {
            return UIColor.init(hexa: 0xcdf7dd)
        }
        
        open class var successTextGreen: UIColor {
            return UIColor.init(hexa: 0x22bb33)
        }
        
        open class var errorBackgroundRed: UIColor {
            return UIColor.init(hexa: 0xFFE5E6)
        }
        
        open class var errorTextRed: UIColor {
            return UIColor.init(hexa: 0xFF4848)
        }
    }

    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let coreImageColor = self.coreImageColor
        return (coreImageColor.red, coreImageColor.green, coreImageColor.blue, coreImageColor.alpha)
    }
    
    var hexValue: String {
        let redColor = self.components.red
        let greenColor = self.components.green
        let blueColor = self.components.blue
        
        let hexString = String.init(format: "%02X%02X%02X", arguments: [redColor * 255, greenColor * 255, blueColor * 255])
        return hexString
    }
    
    func getSuitableTextColor() -> UIColor {
        let redColor = self.components.red
        let greenColor = self.components.green
        let blueColor = self.components.blue
        
        let colorBrightness = ((redColor * 299) + (greenColor * 587) + (blueColor * 114)) / 1000
        if colorBrightness > 0.5 {
            return UIColor.black
        } else {
            return UIColor.white
        }
    }
}
