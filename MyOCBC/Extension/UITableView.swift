//
//  UITableView.swift
//  MyOCBC
//
//  Created by David Angkasa on 18/07/22.
//

import UIKit

extension UITableView {
    func flexibleHeight() {
        rowHeight = UITableView.automaticDimension
        estimatedRowHeight = 44
    }
}
