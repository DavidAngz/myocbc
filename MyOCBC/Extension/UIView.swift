//
//  UIView.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit
import SnapKit

extension UIView {
    
    public var content: UIView! {
        guard subviews.count != 0 else {
            addContentView(view: UIView())
            return subviews[0]
        }
        return subviews[0]
    }
    
    public func addContentView(view: UIView) {
        if subviews.count == 0 {
            addSubview(view)
        } else {
            insertSubview(view, at: 0)
        }
        subviews[0].snp.makeConstraints({ (make) in
            make.edges.width.equalTo(self)
        })
    }
    
    func addCorners(radius: CGFloat, corners: CACornerMask) {
        setNeedsLayout()
        layoutIfNeeded()
        layer.cornerRadius = radius
        layer.maskedCorners = corners
        layer.masksToBounds = true
    }
    
    public func setLayer(cornerRadius: CGFloat? = nil, borderWidth width: CGFloat? = nil, borderColor color: UIColor? = nil, clipToBounds: Bool = true) {
        setNeedsLayout()
        layoutIfNeeded()
        if let radius = cornerRadius {
            let size = (frame.width == 0 ? frame.height : frame.width) / 2
            layer.cornerRadius = (radius == 0 ? size : radius)
        } else {
            layer.cornerRadius = 0
        }
        
        
        if let width = width {
            layer.borderWidth = width
        }
        if let color = color {
            layer.borderColor = color.cgColor
        }
        
        layer.masksToBounds = clipToBounds
    }
    
    public func setupSubviews(_ views: [UIView]) {
        for item in views {
            addSubview(item)
        }
    }
    
}
