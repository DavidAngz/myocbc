//
//  UIAppearance.swift
//  MyOCBC
//
//  Created by David Angkasa on 16/07/22.
//

import UIKit

extension UIAppearance {
    
    func style(_ completion: @escaping ((Self)->Void)) {
        completion(self)
    }
    
}

